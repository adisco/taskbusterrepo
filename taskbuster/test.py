from django.core.urlresolvers import reverse
from django.test import TestCase

class  TestHomePage(TestCase):
	def  test_uses_index_homepage(self):
		response=self.client.get(reverse("home"))
		
		self.assertTemplateUsed(response,"taskbuster/index.html")

	def test_uses_base_homepage(self):
		response=self.client.get(reverse("home"))
		self.assertTemplateUsed(response,"base.html")	
	
		
